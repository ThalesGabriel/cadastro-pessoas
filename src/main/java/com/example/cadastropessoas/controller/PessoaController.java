package com.example.cadastropessoas.controller;

import javax.validation.Valid;

import com.example.cadastropessoas.DTO.request.PessoaRequestDTO;
import com.example.cadastropessoas.DTO.response.PessoaResponseDTO;
import com.example.cadastropessoas.model.Pessoa;
import com.example.cadastropessoas.service.PessoaService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {
 
    private final PessoaService service;
    
    public PessoaController(PessoaService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<PessoaResponseDTO> salvar(@Valid @RequestBody PessoaRequestDTO pessoaDTO) {
        Pessoa nova_pessoa = service.salvar(pessoaDTO.toEntity());
        return new ResponseEntity<>(PessoaResponseDTO.toResponse(nova_pessoa), HttpStatus.CREATED);
    }
}
