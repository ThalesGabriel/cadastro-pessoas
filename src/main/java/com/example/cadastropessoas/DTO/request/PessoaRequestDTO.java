package com.example.cadastropessoas.DTO.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.example.cadastropessoas.model.Pessoa;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Getter;

@Getter
public class PessoaRequestDTO {

    @NotNull(message = "invalidName")
    private String name;

    @Email(message = "invalidEmail")
    private String email;

    @CPF(message = "invalidCpf")
    private String cpf;
    
    private String birthday;

    @NotNull(message = "invalidPassword")
    private String password;

    public Pessoa toEntity(){
        return new Pessoa(name, email, cpf, password, birthday);
    }
}