package com.example.cadastropessoas.DTO.response;

import com.example.cadastropessoas.model.Pessoa;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class PessoaResponseDTO {
    private Integer id;
    private String name;
    private String email;
    private String birthday;

    public static PessoaResponseDTO toResponse(Pessoa pessoa) {
        return new PessoaResponseDTO(pessoa.getId(), pessoa.getName(), pessoa.getEmail(), pessoa.getBirthday());
    }
}
