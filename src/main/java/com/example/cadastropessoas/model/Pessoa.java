package com.example.cadastropessoas.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
public class Pessoa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String email;
    private String cpf;
    private String password;
    private String birthday;
    

    public Pessoa(String name, String email, String cpf, String password, String birthday) {
        this.name = name;
        this.email = email;
        this.cpf = cpf;
        this.password = password;
        this.birthday = birthday;
    }
}