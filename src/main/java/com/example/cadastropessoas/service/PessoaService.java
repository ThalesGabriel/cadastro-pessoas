package com.example.cadastropessoas.service;

import com.example.cadastropessoas.DAO.PessoaDAO;
import com.example.cadastropessoas.model.Pessoa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {

    private final PessoaDAO repository;

    @Autowired
    public PessoaService(PessoaDAO repository) {
        this.repository = repository;
    }

    public Pessoa salvar(Pessoa pessoa) {
        return repository.save(pessoa);
    }
}