package com.example.cadastropessoas.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.cadastropessoas.model.Pessoa;

@Repository
public interface PessoaDAO extends JpaRepository<Pessoa, Integer>{}